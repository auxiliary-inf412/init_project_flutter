import 'package:flutter/material.dart';
import 'package:init_project_flutter/pages/home_page.dart';
import 'package:init_project_flutter/pages/map_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: 'map_page',
      routes: {'home_page': (context) => const MyHomePage(), 'map_page': (context) => const MapPage()},
    );
  }
}

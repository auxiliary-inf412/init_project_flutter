import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

class MapPage extends StatefulWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Position? position;

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    //
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Map Page"),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
      ),
      body: FlutterMap(
        options: MapOptions(
          center: LatLng(-17.7762548, -63.1961685),
          zoom: 14,
          onTap: (tapPosition, point) {
            print("POSITION OBTAINED: ${point.latitude},${point.longitude}");
          },
        ),
        children: [
          TileLayer(
            urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
          ),
          MarkerLayer(
            markers: [
              Marker(
                  point: position == null ? LatLng(-17.7762548, -63.1961685) : LatLng(position!.latitude, position!.longitude),
                  builder: (context) => Icon(Icons.location_on))
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Position position = await _determinePosition();
            setState(() {
              position = position;
            });
            print("POS: ${position.latitude}, ${position.longitude}");
          },
          child: Icon(Icons.location_city)),
    );
  }
}
